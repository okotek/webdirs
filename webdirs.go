package webdirs

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	okolog "../okolog"
	sec "../sec"
	types "../types"

	//abug "../abug"
	"gocv.io/x/gocv"
)

//HandleMeta responds to a request for a JSON encoded list of all appropriate files
//that the front end then requests from the server.
func HandleMeta() func(writer http.ResponseWriter, request *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {

		var decReq types.ImageDetails //Decoded Request
		var err error
		var imageList string

		err = json.NewDecoder(request.Body).Decode(&decReq)
		if err != nil {
			fmt.Println("Error: ", err)
		}

		imageList = generateImageList(decReq)
		//fmt.Println("ImageList:", imageList)
		writer.Write([]byte(imageList))
	}
}

//HandleCSS handles serving up the CSS file.
//This will justify having its own function in later releases.
func HandleCSS() func(writer http.ResponseWriter, request *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {
		var cssData []byte
		var err error

		cssData, err = ioutil.ReadFile("static/styles.css")
		if err != nil {
			fmt.Println("Error reading CSS file:", err)
		}

		fmt.Println("Writing CSS to client")

		writer.Header().Set("Content-type", "text/css")
		writer.Write(cssData)
	}
}

//HandleJS same as HandleCSS but for JS
func HandleJS(input string) func(writer http.ResponseWriter, request *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {
		var jsData []byte
		var err error

		if input == "main" {
			jsData, err = ioutil.ReadFile("static/scripts.js")
		} else if input == "JQ" {
			jsData, err = ioutil.ReadFile("static/jquery-3.4.1.js")
		} else if input == "ED" {
			jsData, err = ioutil.ReadFile("static/editor.js")
		} else {
			fmt.Println("Didn't load the right scripts. ")
		}

		if err != nil {
			fmt.Println("Error reading JS file:", err)
		}

		writer.Header().Set("Content-type", "text/javascript")
		writer.Write(jsData)
	}
}

/*HandleArt serves up all of the icons, background images and other related
sylistic non-text data.*/
func HandleArt() func(writer http.ResponseWriter, request *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {

		pBase := request.URL.Path
		fmt.Println("pBase: ", pBase)
		baseExtPre := strings.Split(path.Base(pBase), ".")
		if len(baseExtPre) < 2 {
			//Failure condition
			fmt.Println("NOT WORKING")
			return
		}

		baseExt := baseExtPre[1]

		if baseExt == "jpg" {
			tmpMat := gocv.IMRead("media/"+path.Base(pBase), 1)
			tmpImg, err := gocv.IMEncode(".jpg", tmpMat)

			if err != nil {
				fmt.Println("ERROR: ", err)
			}

			writer.Header().Set("Content-type", "image/jpeg")
			writer.Header().Set("Content-Length", strconv.Itoa(len(tmpImg)))

			fmt.Println("pBase: ", pBase)

			writer.Write(tmpImg)

		} else if baseExt == "png" {
			tmpMat := gocv.IMRead("media/"+path.Base(pBase), 1)
			tmpImg, err := gocv.IMEncode(".png", tmpMat)

			if err != nil {
				fmt.Println("ERROR: ", err)
			}

			writer.Header().Set("Content-type", "image/png")
			writer.Header().Set("Content-Length", strconv.Itoa(len(tmpImg)))
			fmt.Println("pBase: ", pBase)

			writer.Write(tmpImg)

		} else {
			//bad filetype handler
			fmt.Println("Bad filetype handler: ", baseExt)
		}

	}
}

/*HandleRoot serves the root page of the webapp and handles authentication
of the user. It also provides a list of image names for the page to request,
specified by variables the user sets.*/
func HandleRoot() func(writer http.ResponseWriter, request *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {

		var htmlText []byte
		var error500 []byte

		var err error

		htmlText, err = ioutil.ReadFile("static/index.html")
		error500, err = ioutil.ReadFile("404.html")

		if err != nil {
			writer.Write(error500)
		}

		writer.Write([]byte( /*finalMarkup*/ htmlText))

	}
}

/*
	HandleImages serves the images that are requested via jQuery AJAX requests
	from the root page.
*/
func HandleImages() func(writer http.ResponseWriter, request *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {

		var decReq types.ImageDetails
		var err error
		var tmpMat gocv.Mat

		err = json.NewDecoder(request.Body).Decode(&decReq)
		if err != nil {
			fmt.Println("Error in JSON decode: ", err)
		}

		fmt.Println("=====================\ndecReq: ", decReq)
		fmt.Println("request.URL.Path: ", request.URL.Path)
		fmt.Println("path.Base(request.URL.Path): ", path.Base(request.URL.Path), "\n=====================")

		tmp1 := strings.SplitAfter(request.URL.Path, "/")
		user := tmp1[2]

		fmt.Println("TMP!", tmp1)

		//DOES THIS FILE ACTUALLY EXIST?
		if fileExists("users/" + decReq.Username + user + path.Base(request.URL.Path)) {
			tmpMat = gocv.IMRead("users/"+decReq.Username+user+path.Base(request.URL.Path), 1)
		} else {
			tmpMat = gocv.IMRead("media/missing.jpg", 1)
		}

		tmpImg, err := gocv.IMEncode(".jpg", tmpMat)

		if err != nil {
			fmt.Print("\n\n\n\n=======================\n", err, "\n\n\n")
		}

		writer.Header().Set("Content-type", "image/jpeg")
		writer.Header().Set("Content-Length", strconv.Itoa(len(tmpImg)))

		writer.Write([]byte(tmpImg))
	}
}

func generateJpgImageFromFrame(inputFrame types.Frame) []byte {

	interMat := types.CreateMatFromFrameSimp(inputFrame)
	finImage, _ := gocv.IMEncode(".jpg", interMat)
	return finImage
}

func splitThing(input string) (string, string) {

	var uname, camTag string
	tmpslc := strings.Split(input, "^")
	uname, camTag = tmpslc[0], tmpslc[1]

	return uname, camTag
}

//This function generates a comma-separated list of image
//names for the frontend scripts to request.
func generateImageList(input types.ImageDetails) string {
	var checkHash []byte
	var err error
	var check bool = false
	var dirList []string
	var earlyTimestamp int64
	var lateTimestamp int64
	var retList []string

	//Mon Jan 2 15:04:05 -0700 MST 2006
	//2020-04-15
	//2006-01-02 00:00:00
	t1, _ := time.Parse("2006-01-02", input.OldImg)
	t2, _ := time.Parse("2006-01-02", input.NewImg)

	earlyTimestamp = t1.UnixNano()
	lateTimestamp = t2.UnixNano()

	//Check password by reading "hashfile.txt" in the user directory
	checkHash, err = ioutil.ReadFile("users/" + input.Username + "/" + "hashFile.txt")
	if err == nil {
		//fmt.Println("inputPassword: ", input.Password)
		check = sec.ComparePasswords(string(checkHash), []byte(input.Password))
	} else {
		fmt.Println(err)
	}

	if !check {
		return "\"Password Error\""
	}

	dirList, err = filepath.Glob("users/" + input.Username + "/" + "*.jpg")
	if err != nil {
		fmt.Println("Error:", err)
		return "\"Username Not Found\""
	}

	for count := range dirList {
		dirList[count] = strings.TrimPrefix(dirList[count], "users/"+input.Username+"/")
	}

	for _, iter := range dirList {
		imDate := strings.Split(iter, "|")
		strDate := strings.Split(imDate[1], ".")

		fmt.Println("imDate: ", imDate)

		if imageTime, err := strconv.ParseInt(strDate[0], 10, 64); err == nil {

			if imageTime > earlyTimestamp && imageTime < lateTimestamp {
				retList = append(retList, iter)
			}
		} else {
			//return "\"No Images Within Date Range Found\""
			fmt.Print("Skipping on that date.\r")
		}
	}

	retStr := strings.Join(retList, ",")
	return retStr
}

// Quick utility function
func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

//PlainHTML is a generic HTML loader
func PlainHTML(target string) func(writer http.ResponseWriter, request *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {

		fDat, err := ioutil.ReadFile(target)
		if err != nil {
			fmt.Println("Could not open", target, "due to error:", err)
			writer.Write([]byte("404lol"))
		} else {
			writer.Write([]byte(fDat))
		}
	}
}

/*
	LiveStreamImageHandler sends the image data to the livestream page as it requests them
	via the javascript application. The requested image details are encoded into the url, which
	is broken up into (username)/(id of the camera unit)/(id of the camera). This is then
	used to pull data from a map of frames which is then encoded and sent out to the livestream.

	okokit.tk/lsimg/username/unitid/camID
	okokit.tk/lsimg/ninjamike/4/3
*/
func LiveStreamImageHandler(liveBuffer *map[string]types.LiveStoreElement, timeBuffer *map[string]time.Time) func(writer http.ResponseWriter, request *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {

		var debg bool = true

		lsBuffer := *liveBuffer
		tmpTime := *timeBuffer
		var err error
		var tmpMat gocv.Mat

		tmpPath := request.URL.Path              // "/ninjamike/4/3"
		breakdown := strings.Split(tmpPath, "/") // [ninjamike, 4, 3]

		okolog.MessageLogger("LivestreamImageHandler breakdown var: " + strCat(breakdown))

		if debg {
			fmt.Println("Breakdown tmpPath: ", breakdown, ": ", tmpPath)
		}

		// make sure the format of the URL is right, then break it up. Unitid is not used yet.
		if len(breakdown) != 3 {
			okolog.MessageLogger("Length of breakdown is not 3. Breakdown: " + strCat(breakdown))
			return
		}
		uname, _, cameraid, _ := checkBreakdown(breakdown)
		go func() {
			tmpTime[uname] = time.Now()
			*timeBuffer = tmpTime
			if debg {
				fmt.Println("Sent to timeBuffer")
			}
		}()

		//Get the list of live frames
		tmpFrameList := lsBuffer[uname]

		//Pull the frame for that particular camera
		tmpFrame, err := getFrameByCam(cameraid, tmpFrameList)

		//Turn the frame into an opencv mat
		tmpMat = types.CreateMatFromFrameSimp(tmpFrame)

		//Encode that mat as a jpg
		tmpImg, err := gocv.IMEncode(".jpg", tmpMat)

		if err != nil {
			fmt.Print("\n\n\n\n=======================\n", err, "\n\n\n")
			return
		}

		writer.Header().Set("Content-type", "image/jpeg")
		writer.Header().Set("Content-Length", strconv.Itoa(len(tmpImg)))

		writer.Write([]byte(tmpImg))

	}
}

/*
	LiveStreamImageHandler sends the image data to the livestream page as it requests them
	via the javascript application. The requested image details are encoded into the url, which
	is broken up into (username)/(id of the camera unit)/(id of the camera). This is then
	used to pull data from a map of frames which is then encoded and sent out to the livestream.

	okokit.tk/lsimg/username/unitid/camID
	okokit.tk/lsimg/ninjamike/4/3
*/
func _LiveStreamImageHandler(liveBufferUpdateChannel chan map[string]types.LiveStoreElement, timeChan chan map[string]time.Time) func(writer http.ResponseWriter, request *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {

		lsBuffer := <-liveBufferUpdateChannel
		tmpTime := <-timeChan
		var err error
		var tmpMat gocv.Mat

		tmpPath := request.URL.Path              // "/ninjamike/4/3"
		breakdown := strings.Split(tmpPath, "/") // [ninjamike, 4, 3]

		// make sure the format of the URL is right, then break it up. Unitid is not used yet.
		uname, _, cameraid, _ := checkBreakdown(breakdown)
		go func() {
			tmpTime[uname] = time.Now()
			timeChan <- tmpTime
		}()

		//Get the list of live frames
		tmpFrameList := lsBuffer[uname]

		//fmt.Println("In En")

		//Pull the frame for that particular camera
		tmpFrame, _ := getFrameByCam(cameraid, tmpFrameList)

		//Turn the frame into an opencv mat
		tmpMat = types.CreateMatFromFrameSimp(tmpFrame)

		//Encode that mat as a jpg
		tmpImg, err := gocv.IMEncode(".jpg", tmpMat)

		if err != nil {
			fmt.Print("\n\n\n\n=======================\n", err, "\n\n\n")
			return
		}

		writer.Header().Set("Content-type", "image/jpeg")
		writer.Header().Set("Content-Length", strconv.Itoa(len(tmpImg)))

		writer.Write([]byte(tmpImg))

	}
}

/*
	Takes a plain string and uses regexp to check the layout of it, then returns a username, unitid, and cameraid

func _checkBreakdown(input []string) (string, string, string) {

}
*/
/*
	CheckBreakdown makes sure that the string list is
	a 3 element long list of strings where the second and
	third element is numeric and the first is a username.
*/
func checkBreakdown(input []string) (string, string, string, error) {
	reterr := errors.New("Input length for checkBreakdown() was not equal to 3.")

	//TODO: Handle this better:
	if len(input) != 3 {
		fmt.Println("checkBreakdown function input length was not equal to 3")
		return "", "", "", reterr
	}

	//Regexes instead of checkNumeric() would be good here, assuming performance isn't too terribly affected
	if len(input) == 3 && checkNumeric(input[1]) && checkNumeric(input[2]) {
		return input[0], input[1], input[2], nil
	} else if len(input) > 3 && checkNumeric(input[1]) && checkNumeric(input[2]) { //In the case of extra directories being accidentally tagged on
		return input[0], input[1], input[2], nil
	} else if len(input) > 3 && checkNumeric(input[len(input)-1]) && checkNumeric(input[len(input)]) {
		return input[len(input)-2], input[len(input)-1], input[len(input)], nil
	} else {
		return "", "", "", reterr
		//Try to recover the data, even if it's formatted inproperly.
	}

}

/*

 */

func getFrameByCam(targName string, frameList types.LiveStoreElement) (types.Frame, error) {

	for _, iter := range frameList { //Iterate over the frames in this livestore element
		if iter.CamID == targName {
			return iter, nil
		}
	}

	var fCat string = ""
	for iter := range frameList {
		fCat = fCat + iter
	}

	if fCat == "" {
		fCat = "\"NONE\""
	}

	erret := fmt.Errorf("Returning blank frame on getFrameByCam. camid: %s    available ids: %v ", targName, fCat)
	fmt.Println("===================================\nRETURNING BLANK FRAME\n==========================================")
	var blankframe types.Frame = types.CreateBlankFrame()
	return blankframe, erret

}

//48-57
//Check if input is a number string and not something else
func checkNumeric(input string) bool {

	var retval bool = true

	for _, iter := range input {
		if iter < 48 || iter > 57 {
			retval = false
		}
	}

	return retval
}

func HandleLSPage() func(writer http.ResponseWriter, request *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {

		fDat, err := ioutil.ReadFile("static/livestream.html")
		if err != nil {
			fmt.Println("Could not open livestream.html due to error:", err)
			writer.Write([]byte("404lol"))
		} else {
			writer.Write([]byte(fDat))
		}
	}
}

func strCat(input []string) string {
	var ret string
	for _, line := range input {
		ret = ret + line
	}

	return ret
}
